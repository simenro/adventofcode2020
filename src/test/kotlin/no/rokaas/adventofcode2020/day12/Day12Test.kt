package no.rokaas.adventofcode2020.day12

import no.rokaas.adventofcode2020.day12.Day12.Part.PART1
import no.rokaas.adventofcode2020.day12.Day12.Part.PART2
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day12Test {

    @Test
    internal fun testPart1() {
        assertThat(Day12().solvePart("/day12-test.txt", PART1)).isEqualTo(25)
        println()
    }

    @Test
    internal fun testPart2() {
        assertThat(Day12().solvePart("/day12-test.txt", PART2)).isEqualTo(286)
    }

}