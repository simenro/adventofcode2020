package no.rokaas.adventofcode2020.day13

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day13Test {

    @Test
    fun testPart1() {
        Assertions.assertThat(Day13().solvePart1("/day13-test.txt")).isEqualTo(295)
        println()
    }

    @Test
    fun testPart2() {
        Assertions.assertThat(Day13().solvePart2("/day13-test.txt")).isEqualTo(1068781)
    }

}