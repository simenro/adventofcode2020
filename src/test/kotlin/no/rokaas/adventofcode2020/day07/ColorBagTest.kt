package no.rokaas.adventofcode2020.day07

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ColorBagTest {

    @Test
    fun parse() {
        val bags = ColorBag.parse(
            listOf(
                "dotted blue bags contain 3 wavy beige bags, 5 clear tomato bags.",
                "wavy beige bags contain 1 shiny gold bag, 1 dark orange bag, 4 muted lavender bags.",
                "dark orange bags contain no other bags."
            )
        )
        val dottedBlue = bags["dotted blue"]!!
        assertThat(dottedBlue.inner["wavy beige"]!!.second).isEqualTo(3)
        assertThat(dottedBlue.inner["clear tomato"]!!.second).isEqualTo(5)

        assertThat(dottedBlue.inner["wavy beige"]!!.first.inner["shiny gold"]!!.second).isEqualTo(1)

        val wavyBeige = bags["wavy beige"]!!
        assertThat(wavyBeige.inner["shiny gold"]!!.second).isEqualTo(1)
        assertThat(wavyBeige.inner["dark orange"]!!.second).isEqualTo(1)
        assertThat(wavyBeige.inner["muted lavender"]!!.second).isEqualTo(4)

        assertThat(ColorBag.findBagColorsEventuallyContaining(bags, "shiny gold")).hasSize(2)
    }

    @Test
    fun parseBagWithoutInnerBags() {
        val bag = ColorBag.parse(listOf("dotted black bags contain no other bags."))
        assertThat(bag).containsOnlyKeys("dotted black")
        assertThat(bag["dotted black"]!!.inner).isEmpty()
    }

    @Test
    fun countContainedBags() {
        val bags = ColorBag.parse(
            listOf(
                "shiny gold bags contain 2 dotted blue bags, 4 wavy beige bags, 1 dotted black bag.",
                "dotted blue bags contain 3 wavy beige bags, 5 clear tomato bags.",
                "wavy beige bags contain 1 dark orange bag, 4 muted lavender bags.",
                "muted lavender bags contain 3 dotted black bags.",
                "dotted black bags contain no other bags."
            )
        )

        val mutedLavender = 3
        val wavyBeige = 1 + 4 + 4 * mutedLavender // 17
        val dottedBlue = 3 + 3 * wavyBeige + 5 // 59
        val shinyGold = 2 + 2 * dottedBlue + 4 + 4 * wavyBeige + 1 // 193

        assertThat(dottedBlue).isEqualTo(59)
        assertThat(shinyGold).isEqualTo(2 + 2 * 59 + 4 + 4 * 17 + 1)

        assertThat(bags["dotted black"]?.countRequiredBags(bags)).isEqualTo(0)
        assertThat(bags["muted lavender"]?.countRequiredBags(bags)).isEqualTo(3)
        assertThat(bags["wavy beige"]?.countRequiredBags(bags)).isEqualTo(17)
        assertThat(bags["dotted blue"]?.countRequiredBags(bags)).isEqualTo(59)
        assertThat(bags["shiny gold"]?.countRequiredBags(bags)).isEqualTo(193)
    }
}