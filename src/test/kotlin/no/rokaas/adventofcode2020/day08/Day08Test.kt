package no.rokaas.adventofcode2020.day08

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day08Test {

    @Test
    fun runBootCode() {
        assertThat(Day08().solvePart1("/day08-test.txt")).isEqualTo(5)
    }
}