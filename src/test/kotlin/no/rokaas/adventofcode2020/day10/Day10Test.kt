package no.rokaas.adventofcode2020.day10

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

class Day10Test {

    @Test
    internal fun testPart1() {
        Assertions.assertThat(Day10().solvePart1("/day10-test.txt")).isEqualTo(35)
    }

    @Test
    internal fun testPart2() {
        Assertions.assertThat(Day10().solvePart2("/day10-test.txt")).isEqualTo(8)
    }

}