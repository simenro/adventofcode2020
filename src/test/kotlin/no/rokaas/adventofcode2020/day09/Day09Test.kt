package no.rokaas.adventofcode2020.day09

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day09Test {

    @Test
    internal fun test() {
        assertThat(Day09().solvePart1("/day09-test.txt", 5)).isEqualTo(127)
        assertThat(Day09().solvePart2("/day09-test.txt", 127)).isEqualTo(62)
    }
}