package no.rokaas.adventofcode2020.day11

import no.rokaas.adventofcode2020.day11.Day11.Part.PART1
import no.rokaas.adventofcode2020.day11.Day11.Part.PART2
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class Day11Test {

    @Test
    internal fun testPart1() {
        assertThat(Day11().solvePart("/day11-test.txt", PART1)).isEqualTo(37)
    }

    @Test
    internal fun testPart2() {
        assertThat(Day11().solvePart("/day11-test.txt", PART2)).isEqualTo(26)
    }

    @Test
    internal fun testNeighborsInSight() {
        val matrix = Matrix(Day11().getInput("/day11-2-test.txt"))
        assertThat(matrix.insight(4, 3, oneStep = true)).isEqualTo(2)
        assertThat(matrix.insight(4, 3)).isEqualTo(8)
    }
}