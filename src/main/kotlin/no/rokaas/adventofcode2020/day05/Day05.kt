package no.rokaas.adventofcode2020.day05

import java.io.File

fun main() {
    Day05().solve()
}

class Day05 {
    fun solve() {
        val boardingPassList = getInput()
        //println(boardingPassList[0])
        val seatList = boardingPassList.map { bp -> bp.seat }
        val min = seatList.minOrNull()!!
        val max = seatList.maxOrNull()!!
        println("Min & max seat: $min & $max")
        IntRange(min, max).forEach {
            if (it !in seatList) {
                println("Missing: $it")
            }
        }
    }

    private fun getInput(): List<BoardingPass> {
        return File(Day05::class.java.getResource("/day05.txt").toURI()).readLines()
            .map { BoardingPass.parse(it) }
    }

}