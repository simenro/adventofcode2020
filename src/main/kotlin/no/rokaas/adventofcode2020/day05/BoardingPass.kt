package no.rokaas.adventofcode2020.day05

data class BoardingPass(val line: String, val row: Int, val column: Int, val seat: Int) {

    companion object {
        fun parse(line: String): BoardingPass {
            val binaryLine = line.replace("""[BR]""".toRegex(), "1")
                .replace("""[FL]""".toRegex(), "0")
            val row = Integer.parseUnsignedInt(binaryLine.substring(0, 7), 2)
            val col = Integer.parseUnsignedInt(binaryLine.substring(7, 10), 2)
            return BoardingPass(line, row, col, (row * 8) + col)
        }
    }
}