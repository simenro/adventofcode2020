package no.rokaas.adventofcode2020.day11

import no.rokaas.adventofcode2020.day11.Day11.Part.PART1
import java.io.File

fun main() {
    var occupiedSeats = Day11().solvePart("/day11.txt", PART1)
    println("Number of occupied seats, part 1: $occupiedSeats")
    occupiedSeats = Day11().solvePart("/day11.txt", Day11.Part.PART2)
    println("Number of occupied seats, part 2: $occupiedSeats")
}

val debug = false

class Day11 {

    fun solvePart(inputFile: String, part: Part): Int {
        var matrix = getInput(inputFile)
        printMatrix(matrix)

        var numTics = 1
        do {
            val (matrixChanges, newMatrix) = tic(matrix, part)
            matrix = newMatrix
            println("Tic $numTics: $matrixChanges changes")
            printMatrix(matrix)
            numTics++
        } while (matrixChanges > 0)
        return countOccupied(matrix)
    }

    private fun tic(matrix: Array<Array<Char>>, part: Part): Pair<Int, Array<Array<Char>>> {
        val new = Array(matrix.size) { Array(matrix[0].size) { '.' } }
        var numChanges = 0
        val occupationLimit = if (part == PART1) 4 else 5
        val mx = Matrix(matrix)
        matrix.indices.forEach { row ->
            matrix[row].indices.forEach { col ->
                new[row][col] = matrix[row][col]
                val occ = mx.insight(row, col, part == PART1)
                if (matrix[row][col] == 'L' && occ == 0) {
                    new[row][col] = '#'
                    numChanges++
                } else if (matrix[row][col] == '#' && occ >= occupationLimit) {
                    new[row][col] = 'L'
                    numChanges++
                }
            }
        }
        return Pair(numChanges, new)
    }

    private fun countOccupied(matrix: Array<Array<Char>>): Int {
        return matrix.map { it.count { ch -> ch == '#' } }.sum()
    }

    fun getInput(inputFile: String): Array<Array<Char>> {
        val lines = File(Day11::class.java.getResource(inputFile).toURI()).readLines()
        // get dimensions
        val numRows = lines.size
        val numCols = lines[0].toCharArray().size
        println("Seat layout is $numRows x $numCols (rows x cols)")
        val matrix = Array(numRows) { Array(numCols) { '.' } }
        var row = 0
        lines.forEach { line ->
            matrix[row++] = line.toCharArray().toTypedArray()
        }
        return matrix
    }

    private fun printMatrix(matrix: Array<Array<Char>>) {
        if (!debug) return
        for (idx: Int in matrix.indices) {
            val row = matrix[idx]
            for (col: Int in row.indices) {
                print("${row[col]} ")
            }
            print("\n")
        }
    }

    enum class Part { PART1, PART2 }
}