package no.rokaas.adventofcode2020.day11

import no.rokaas.adventofcode2020.day11.Direction.*

enum class Direction {
    UP, DOWN, LEFT, RIGHT,
    UPRIGHT, UPLEFT, DOWNLEFT, DOWNRIGHT
}

data class Matrix(val inputArr: Array<Array<Char>>) {

    private val arr = expand()

    fun get(x: Int, y: Int): Char = arr[x][y]

    private fun expand(): Array<Array<Char>> {
        val numRows = inputArr.size
        val numCols = inputArr[0].size
        val output = Array(numRows + 2) { Array(numCols + 2) { 'S' } }
        for (i in 1..numRows) {
            for (j in 1..numCols) {
                output[i][j] = inputArr[i - 1][j - 1]
            }
        }
        return output
    }

    fun insight(x: Int, y: Int, oneStep: Boolean = false) =
        Direction.values().map { insight(x, y, it, oneStep) }.sum()

    private fun insight(x: Int, y: Int, dir: Direction, oneStep: Boolean = false): Int {
        var xx = x + 1
        var yy = y + 1
        do {
            if (dir in listOf(LEFT, UPLEFT, DOWNLEFT)) xx--
            if (dir in listOf(RIGHT, UPRIGHT, DOWNRIGHT)) xx++
            if (dir in listOf(UP, UPRIGHT, UPLEFT)) yy--
            if (dir in listOf(DOWN, DOWNRIGHT, DOWNLEFT)) yy++
            if (arr[xx][yy] == '#') return 1
        } while (arr[xx][yy] == '.' && !oneStep)
        return 0
    }
}