package no.rokaas.adventofcode2020.day08

data class InstructionLine(var instruction: Instruction, val argument: Int, var numVisits: Int) {
    override fun toString(): String {
        return "$instruction $argument ($numVisits)"
    }
}

enum class Instruction {
    ACC, JMP, NOP
}