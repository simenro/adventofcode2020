package no.rokaas.adventofcode2020.day08

import no.rokaas.adventofcode2020.day08.Instruction.*
import java.io.File

fun main() {
    Day08().solvePart1("/day08.txt")
    Day08().solvePart2("/day08.txt")
}

class Day08 {
    fun solvePart1(inputFile: String): Int {
        val instructions = getInput(inputFile)
        println("Found ${instructions.size} instructions.")
        //println(instructions.toString().replace(",", "\n"))
        val accResult = runBootCode(instructions).second
        println("Accumulator result: $accResult")
        return accResult
    }

    fun solvePart2(inputFile: String): Int {
        val instructions = getInput(inputFile)
        val numJumps = instructions.values.filter { it.instruction == JMP }.size
        val numNops = instructions.values.filter { it.instruction == NOP }.size
        println("Found ${instructions.size} instructions with $numJumps JMPs and $numNops NOPs.")
        var result = replace(instructions, JMP, NOP)
        if (result == 0) {
            result = replace(instructions, NOP, JMP)
        }
        return result
    }

    private fun replace(instructions: Map<Int, InstructionLine>, fromInstr: Instruction, toInstr: Instruction): Int {
        val instrs = instructions.filter { it.value.instruction == fromInstr }
        val instr = instrs.iterator()
        while (instr.hasNext()) {
            val instrNo = instr.next().key
            println("Replacing $fromInstr at line $instrNo...")
            instructions[instrNo]!!.instruction = toInstr
            val (success, acc) = runBootCode(instructions)
            if (success) {
                println("Accumulator result after fixing $fromInstr to $toInstr at line $instrNo: $acc")
                return acc
            }
            instructions[instrNo]!!.instruction = fromInstr
            instructions.values.forEach { it.numVisits = 0 }
        }
        println("No dice...")
        return 0
    }

    private fun runBootCode(instructions: Map<Int, InstructionLine>): Pair<Boolean, Int> {
        var acc = 0
        var instrNo = 1
        while (true) {
            val instr = instructions[instrNo]!!
            //print("${step++} Processing instruction $instrNo: $instr ")
            if (instr.numVisits >= 1) {
                println("Instruction already visited - stopping early at line $instrNo.")
                return Pair(false, acc)
            }
            instr.numVisits++
            when (instr.instruction) {
                ACC -> { acc += instr.argument; instrNo++ }
                JMP -> instrNo += instr.argument
                NOP -> instrNo++
            }
            //println("Acc: $acc")
            if (instrNo > instructions.size) {
                println("Last instruction processed - normal termination.")
                return Pair(true, acc)
            }
        }
    }

    private fun getInput(inputFile: String): Map<Int, InstructionLine> {
        var step = 1
        return File(Day08::class.java.getResource(inputFile).toURI()).readLines()
            .map { line ->
                val parts = line.split(" ")
                InstructionLine(Instruction.valueOf(parts[0].toUpperCase()), parts[1].toInt(), 0)
            }.associateBy { step++ }
    }

}