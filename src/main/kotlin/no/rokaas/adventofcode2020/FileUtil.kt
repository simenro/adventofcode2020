package no.rokaas.adventofcode2020

import java.io.File

class FileUtil {
    companion object {
        fun readLines(filename: String) = File({}.javaClass.getResource(filename).toURI()).readLines()
    }
}