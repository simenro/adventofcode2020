package no.rokaas.adventofcode2020.day01

import java.io.File

fun main() {
    println(Day01().solve1())
    println(Day01().solve2())
}

class Day01 {
    fun solve1(): Int {
        val map = HashMap<Int, Int>()
        val input = getInput1()
        input.forEach { num1 ->
            input.filter { num2 -> num1 != num2 && num1 + num2 == 2020 }
                .forEach { num2 -> map[num1] = num2 }
        }
        println("map: $map")
        map.entries.first { e ->
            return e.key * e.value
        }
        return -1
    }

    fun solve2(): Int {
        val list = ArrayList<Int>()
        val input = getInput1()
        input.forEach { num1 ->
            input.forEach { num2 ->
                input.filter { num3 -> num1 != num2 && num1 != num3 && num2 != num3 && num1 + num2 + num3 == 2020 }
                    .forEach { num3 ->
                        list.add(num1)
                        list.add(num2)
                        list.add(num3)
                    }
            }
        }
        println("list: $list")
        return list[0] * list[1] * list[2]
    }

    private fun getInput1(): List<Int> {
        return File(Day01::class.java.getResource("/day01.txt").toURI()).readLines()
            .map { it.toInt() }
    }
}
