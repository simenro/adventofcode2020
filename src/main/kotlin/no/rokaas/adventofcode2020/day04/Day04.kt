package no.rokaas.adventofcode2020.day04

import java.io.File

fun main() {
    Day04().solve()
}

class Day04 {
    fun solve() {
        val passports = getInput()
        println("# passports: ${passports.size}")
        passports.forEach { pp -> println("$pp - ${pp.isValid()}") }
        val numValid = passports.count { pp -> pp.isValid() }
        println("# valid passports: $numValid")
    }

    private fun getInput(): List<Passport> {
        val lines = File(Day04::class.java.getResource("/day04.txt").toURI()).readLines()
        val passports = ArrayList<Passport>()
        val ppLines = ArrayList<String>()
        lines.forEach { line ->
            if (line.isEmpty()) {
                passports.add(Passport.parse(ppLines))
                ppLines.clear()
            } else {
                ppLines.add(line)
            }
        }
        if (ppLines.isNotEmpty()) {
            passports.add(Passport.parse(ppLines))
        }
        return passports
    }

}