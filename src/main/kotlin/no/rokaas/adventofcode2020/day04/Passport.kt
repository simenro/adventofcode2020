package no.rokaas.adventofcode2020.day04

data class Passport(
    var birthYear: String = "",
    var issueYear: String = "",
    var expirationYear: String = "",
    var height: String = "",
    var hairColor: String = "",
    var eyeColor: String = "",
    var passportId: String = "",
    var countryId: String? = ""
) {
    companion object {
        fun parse(lines: List<String>): Passport {
            val keyValueList = ArrayList<String>()
            lines.forEach { line ->
                keyValueList.addAll(line.split(" "))
            }
            val pp = Passport()
            keyValueList.forEach { item ->
                val parts = item.split(":")
                when (parts[0]) {
                    "byr" -> pp.birthYear = parts[1]
                    "iyr" -> pp.issueYear = parts[1]
                    "eyr" -> pp.expirationYear = parts[1]
                    "hgt" -> pp.height = parts[1]
                    "hcl" -> pp.hairColor = parts[1]
                    "ecl" -> pp.eyeColor = parts[1]
                    "pid" -> pp.passportId = parts[1]
                    "cid" -> pp.countryId = parts[1]
                }
            }
            return pp
        }
    }

    fun isValidLax(): Boolean {
        return birthYear.isNotBlank() &&
                issueYear.isNotBlank() &&
                expirationYear.isNotBlank() &&
                height.isNotBlank() &&
                hairColor.isNotBlank() &&
                eyeColor.isNotBlank() &&
                passportId.isNotBlank()
    }

    fun isValid(): Boolean {
        if (!isValidLax()) {
            return false
        }
        try {
            return birthYear.toInt() in 1920..2002 &&
                    issueYear.toInt() in 2010..2020 &&
                    expirationYear.toInt() in 2020..2030 &&
                    validHeight() &&
                    hairColor.matches("""#[0-9a-f]{6}""".toRegex()) &&
                    eyeColor.matches("""amb|blu|brn|gry|grn|hzl|oth""".toRegex()) &&
                    passportId.matches("""\d{9}""".toRegex())
        } catch (e: Exception) {
            // ignored
        }
        return false
    }

    private fun validHeight(): Boolean {
        if (height.endsWith("cm")) {
            if (height.substringBefore("cm").toInt() in 150..193) return true
        } else if (height.endsWith("in")) {
            if (height.substringBefore("in").toInt() in 59..76) return true
        }
        return false
    }
}
