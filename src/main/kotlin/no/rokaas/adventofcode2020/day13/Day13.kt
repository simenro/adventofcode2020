package no.rokaas.adventofcode2020.day13

import no.rokaas.adventofcode2020.FileUtil.Companion.readLines

fun main() {
    val answer = Day13().solvePart1("/day13.txt")
    println("Answer, part 1: $answer")
    val longAnswer = Day13().solvePart2("/day13.txt")
    println("Answer, part 2: $longAnswer")
}

class Day13 {

    fun solvePart1(inputFile: String): Int {
        val data = getInput(inputFile)
        println("Bus data: $data")
        // compute map of route -> first timestamp after target timestamp which is divisible by route id
        val map = data.ids
            .associateWith { (data.timestamp / it) + 1}
            .mapValues { it.key * it.value }
        // compute waiting times: simply the current values minus target timestamp
        val waitingTimes = map.mapValues { it.value - data.timestamp }
        // answer is the route (key) of the minimum value, multiplied by that route's id
        val min = waitingTimes.minByOrNull { it.value }!!
        println("Map: $map")
        println("Waiting times: $waitingTimes")
        println("Route with minimum waiting time: ${min.key} = ${min.value} min")
        return min.key * min.value
    }

    fun solvePart2(inputFile: String): Long {
        val data = getRouteOffsets(inputFile)
        println("Route offsets: $data")
        // algorithm: given route offsets {0=7, 1=13, 4=59, 6=31, 7=19},
        // find lowest x such that
        // (x + 0) ≡ 0 (mod 7)
        // (x + 1) ≡ 0 (mod 13)
        // (x + 4) ≡ 0 (mod 59)
        // and so on.

        var xStep = data[0]!!.toLong()
        var idx = 1
        val offsets = data.keys.toList()
        val busIds = data.values.toList()
        for (x in xStep until Long.MAX_VALUE step xStep) {
            var success = true
            for (i in 0..idx) {
                if ((x + offsets[i]) % busIds[i] != 0L) {
                    success = false
                    break
                }
            }
            if (success && idx == data.size - 1) return x
            if (success) {
                xStep *= busIds[idx++]
                println("New xStep: $xStep ; idx: $idx")
            }
        }
        return -1
    }

    private fun getInput(inputFile: String): BusData {
        val lines = readLines(inputFile)
        val ids = lines[1].split(",").filterNot { it == "x" }.map { it.toInt() }
        return BusData(lines[0].toInt(), ids)
    }

    private fun getRouteOffsets(inputFile: String): Map<Int, Int> {
        val routes = readLines(inputFile)[1].split(",")
        return routes.indices.associateWith { routes[it] }
            .filterNot { it.value == "x" }
            .mapValues { it.value.toInt() }
    }

}