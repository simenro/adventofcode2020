package no.rokaas.adventofcode2020.day13

data class BusData(val timestamp: Int, val ids: List<Int>)
