package no.rokaas.adventofcode2020.day12

enum class Facing {
    EAST, WEST, NORTH, SOUTH
}