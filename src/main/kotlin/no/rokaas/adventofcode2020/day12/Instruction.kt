package no.rokaas.adventofcode2020.day12

import no.rokaas.adventofcode2020.day12.Day12.Part.PART1
import no.rokaas.adventofcode2020.day12.Day12.Part.PART2
import java.util.ArrayList

data class Instruction(val action: Char, val value: Int) {

    fun advancePosition(waypointPositions: ArrayList<Position>, shipPositions: ArrayList<Position>, part: Day12.Part) {
        when (part) {
            PART1 -> advancePositionPart1(shipPositions)
            PART2 -> advancePositionPart2(waypointPositions, shipPositions)
        }
    }

    private fun advancePositionPart1(shipPositions: ArrayList<Position>) {
        val newPos = shipPositions.last()
        when (action) {
            'N' -> newPos.north += value
            'S' -> newPos.north -= value
            'E' -> newPos.east += value
            'W' -> newPos.east -= value
            'L', 'R' -> newPos.turn(action, value)
            'F' -> newPos.forward(value)
        }
        shipPositions.add(newPos)
        println("After instruction ${toString()}, shipPosition is ${shipPositions.last()}")
    }

    private fun advancePositionPart2(waypointPositions: ArrayList<Position>, shipPositions: ArrayList<Position>) {
        val newShipPos = shipPositions.last()
        val newWaypointPos = waypointPositions.last()
        when (action) {
            'N' -> newWaypointPos.north += value
            'S' -> newWaypointPos.north -= value
            'E' -> newWaypointPos.east += value
            'W' -> newWaypointPos.east -= value
            'L', 'R' -> newWaypointPos.rotate(action, value)
            'F' -> newShipPos.forward(newWaypointPos, value)
        }
        shipPositions.add(newShipPos)
        waypointPositions.add(newWaypointPos)
        println("After instruction ${toString()}, shipPosition is ${shipPositions.last()} and waypointPosition is ${waypointPositions.last()}")
    }

    override fun toString(): String = "$action$value"
}
