package no.rokaas.adventofcode2020.day12

import no.rokaas.adventofcode2020.FileUtil.Companion.readLines
import kotlin.math.abs

fun main() {
    var manhattanDistance = Day12().solvePart("/day12.txt", Day12.Part.PART1)
    println("Manhattan distance, part 1: $manhattanDistance")
    manhattanDistance = Day12().solvePart("/day12.txt", Day12.Part.PART2)
    println("Manhattan distance, part 2: $manhattanDistance")
}

class Day12 {

    fun solvePart(inputFile: String, part: Part): Int {
        val instructions = getInput(inputFile)
        val positions = processInstructions(instructions, part)
        //println("Instructions:\n" + instructions.joinToString("\n"))
        return abs(positions.last().east) + abs(positions.last().north)
    }

    private fun processInstructions(instructions: List<Instruction>, part: Part): List<Position> {
        val shipPositions = ArrayList<Position>(instructions.size)
        val waypointPositions = ArrayList<Position>(instructions.size)
        shipPositions.add(Position())
        waypointPositions.add(Position(east = 10, north = 1)) // only used in part 2
        instructions.forEach { it.advancePosition(waypointPositions, shipPositions, part) }
        return shipPositions
    }

    fun getInput(inputFile: String): List<Instruction> =
        readLines(inputFile).map { Instruction(it[0], it.substring(1).toInt()) }

    enum class Part { PART1, PART2 }
}