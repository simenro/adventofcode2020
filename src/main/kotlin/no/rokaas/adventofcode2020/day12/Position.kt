package no.rokaas.adventofcode2020.day12

import no.rokaas.adventofcode2020.day12.Facing.*

data class Position(var facing: Facing = EAST, var east: Int = 0, var north: Int = 0) {
    // negative east = west, negative north = south

    fun forward(value: Int) {
        when (facing) {
            EAST -> east += value
            WEST -> east -= value
            NORTH -> north += value
            SOUTH -> north -= value
        }
    }

    fun forward(waypointPosition: Position, value: Int) {
        repeat(value) {
            east += waypointPosition.east
            north += waypointPosition.north
        }
    }

    fun turn(direction: Char, degrees: Int) {
        val steps = degrees / 90
        for (step in 1..steps) {
            if (direction == 'L') turnLeft() else turnRight()
        }
    }

    private fun turnLeft() {
        facing = when (facing) {
            EAST -> NORTH
            NORTH -> WEST
            WEST -> SOUTH
            SOUTH -> EAST
        }
    }

    private fun turnRight() {
        facing = when (facing) {
            EAST -> SOUTH
            SOUTH -> WEST
            WEST -> NORTH
            NORTH -> EAST
        }
    }

    fun rotate(direction: Char, degrees: Int) {
        val steps = degrees / 90
        for (step in 1..steps) {
            if (direction == 'L') rotateLeft() else rotateRight()
        }
    }

    private fun rotateLeft() {
        val oldNorth = north
        north = east
        east = -oldNorth
    }

    private fun rotateRight() {
        val oldNorth = north
        north = -east
        east = oldNorth
    }

}
