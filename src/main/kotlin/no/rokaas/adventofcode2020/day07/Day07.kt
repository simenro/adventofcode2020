package no.rokaas.adventofcode2020.day07

import java.io.File

fun main() {
    Day07().solve()
}

class Day07 {
    fun solve() {
        val bags = getInput()
        // bags.forEach { println(it) }
        println("There are ${bags.size} bags.")
        val numEventuallyContaining = ColorBag.findBagColorsEventuallyContaining(bags, "shiny gold")
        println("# bag colors eventually containing at least one shiny gold bag: ${numEventuallyContaining.size}")
        val numRequiredBags = bags["shiny gold"]!!.countRequiredBags(bags)
        println("# bags required in shiny gold bag: $numRequiredBags")
    }

    private fun getInput(): Map<String, ColorBag> {
        return ColorBag.parse(File(Day07::class.java.getResource("/day07.txt").toURI()).readLines())
    }

}