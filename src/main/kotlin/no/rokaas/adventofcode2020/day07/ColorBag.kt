package no.rokaas.adventofcode2020.day07

import java.util.*
import kotlin.collections.HashMap

data class ColorBag(val color: String, var inner: Map<String, Pair<ColorBag, Int>>) {
    companion object {
        fun parse(lines: List<String>): Map<String, ColorBag> {
            val bags = HashMap<String, ColorBag>()
            lines.forEach { line ->
                parseLine(line, bags)
            }
            return bags
        }

        private fun parseLine(line: String, bags: HashMap<String, ColorBag>) {
            // sample line:
            // wavy olive bags contain 4 shiny violet bags, 5 faded tomato bags.
            val parts = line
                .replace(" bags", "")
                .replace(" bag", "")
                .replace(".", "")
                .split(" contain ", ", ")
                .map { it.trim() }
            val color = parts[0]
            val innerMap = HashMap<String, Pair<ColorBag, Int>>()
            if (parts[1] != "no other") {
                parts.drop(1).forEach { part ->
                    val match = "(\\d+) (.*)".toRegex().find(part)!!
                    val (num, clr) = match.destructured
                    // add to all bags if not found
                    if (!bags.containsKey(clr)) {
                        bags[clr] = ColorBag(clr, HashMap())
                    }
                    innerMap[clr] = Pair(bags[clr]!!, num.toInt())
                }
            }
            if (bags.containsKey(color)) {
                bags[color]!!.inner = innerMap
            } else {
                bags[color] = ColorBag(color, innerMap)
            }
        }

        fun findBagColorsEventuallyContaining(allBags: Map<String, ColorBag>, color: String): List<ColorBag> {
            val result = ArrayList<ColorBag>()
            var children = listOf(allBags[color]!!)
            var bags = allBags
            while (true) {
                val parents = findParents(bags.map { it.value }, children)
                if (parents.isEmpty()) {
                    break
                }
                result.addAll(parents)
                children = parents
                bags = bags.filter { b -> !children.map { it.color }.contains(b.key) }
            }
            return result
        }

        // finds all bags in 'bags' having at least one bag from 'children' as innerbag
        private fun findParents(bags: List<ColorBag>, children: List<ColorBag>): List<ColorBag> {
            val childrenKeys = children.map { it.color }
            return bags.filter { bag ->
                bag.inner.keys.intersect(childrenKeys).isNotEmpty()
            }
        }
    }

    // find all bags contained in this bag
    fun countRequiredBags(bags: Map<String, ColorBag>): Int {
        if (inner.isEmpty()) {
            return 0
        }
        val innerBags = inner.values
        var sum = innerBags.map { it.second }.sum()
        // for every inner bag, add the product of its number and innerSum
        innerBags.forEach { (bag, num) ->
            val innerSum = num * bag.countRequiredBags(bags)
            println("innerSum ${bag.color} = $innerSum")
            sum += innerSum
        }
        return sum
    }
}
