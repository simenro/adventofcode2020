package no.rokaas.adventofcode2020.day09

import java.io.File
import java.util.*

fun main() {
    val result = Day09().solvePart1("/day09.txt", 25)
    Day09().solvePart2("/day09.txt", result)
}

class Day09 {
    fun solvePart1(inputFile: String, preambleSize: Int):Long {
        val list = getInput(inputFile)
        println("Input has ${list.size} numbers.")
        val fifo = LinkedList<Long>() as Queue<Long>
        for (idx in list.indices) {
            val current = list[idx]
            if (idx > 0) {
                fifo.offer(list[idx-1])
            }
            if (idx > preambleSize) {
                fifo.poll()
                //println("Step ${idx+1} = $current, fifo = $fifo")
                if (!isSumOfTwo(current, fifo)) {
                    println("First error is $current")
                    return current
                }
            }
        }
        return 0
    }

    private fun isSumOfTwo(num: Long, fifo: Queue<Long>): Boolean {
        fifo.forEach { curr ->
            val others = fifo.filter { it != curr }
            if (others.contains(num - curr)) {
                //println(" OK $curr + ${num - curr}")
                return true
            }
        }
        return false
    }

    fun solvePart2(inputFile: String, sumToMatch: Long): Long {
        val list = getInput(inputFile)
        for (idx in list.indices) {
            var sum = 0L
            var counter = 0
            while (sum < sumToMatch) {
                sum += list[idx + counter++]
            }
            if (sum == sumToMatch) {
                val contiguous = list.subList(idx, idx + --counter)
                val encryptionWeakness = contiguous.minOrNull()!! + contiguous.maxOrNull()!!
                println("Encryption weakness found: $encryptionWeakness")
                return encryptionWeakness
            }
        }
        return 0
    }

    private fun getInput(inputFile: String): List<Long> {
        return File(Day09::class.java.getResource(inputFile).toURI()).readLines()
            .map { it.toLong() }
    }
}