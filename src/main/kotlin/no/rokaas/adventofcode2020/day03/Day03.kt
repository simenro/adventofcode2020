package no.rokaas.adventofcode2020.day03

import java.io.File

fun main() {
    val n1 = Day03().solve(1, 1)
    val n2 = Day03().solve(3, 1)
    val n3 = Day03().solve(5, 1)
    val n4 = Day03().solve(7, 1)
    val n5 = Day03().solve(1, 2)
    val ans = n1*n2*n3*n4*n5
    println("# of trees: $n1 * $n2 * $n3 * $n4 * $n5 = $ans")
}

class Day03 {
    fun solve(xStep: Int, yStep: Int): Long {
        val forest = getInput()
        val forestWidth = forest.lines[0].size
        //println("Forest width (x): $forestWidth")
        //println("Forest depth (y): ${forest.lines.size}")
        var x = 0
        var y = 0
        var numTrees = 0
        while (y < forest.lines.size - 1) {
            x += xStep
            y += yStep
            val currentLine = forest.lines[y]
            val x1 = x.rem(forestWidth)
            val isTree = currentLine[x1] == '#'
            //println("($x->$x1, $y): ${currentLine.contentToString()} = $isTree")
            if (isTree) numTrees++
        }
        return numTrees.toLong()
    }

    private fun getInput() =
        Forest.parse(File(Day03::class.java.getResource("/day03.txt").toURI()).readLines())
}