package no.rokaas.adventofcode2020.day10

import no.rokaas.adventofcode2020.day09.Day09
import java.io.File
import java.lang.Integer.max
import java.util.*
import java.util.stream.IntStream.range
import kotlin.collections.HashMap

fun main() {
    Day10().solvePart1("/day10.txt")
    Day10().solvePart2("/day10.txt")
}

class Day10 {

    fun solvePart1(inputFile: String):Int {
        val diffs = HashMap<Int, Int>() // jolt difference -> count
        val list = getInput(inputFile)
        val builtIn = list.maxOrNull()!! + 3
        list.add(builtIn)
        println("Input has ${list.size} numbers.")
        var current = 0
        while (current < builtIn) {
            val nextAdapter = list.filter { it - current <= 3 }.minOrNull()!!
            val joltDiff = nextAdapter - current
            println("Current jolt output: $current with adapter $nextAdapter yields jolt difference $joltDiff")
            diffs[joltDiff] = diffs[joltDiff]?.plus(1) ?: 1
            list.remove(nextAdapter)
            current = nextAdapter
        }
        val result = diffs.values.reduce { acc, i -> acc * i }
        println("\nResult is the product of $diffs : $result")
        return result
    }

    private fun getInput(inputFile: String): ArrayList<Int> {
        return File(Day09::class.java.getResource(inputFile).toURI()).readLines()
            .map { it.toInt() } as ArrayList<Int>
    }

    fun solvePart2(inputFile: String): Long {
        val listFromFile = getInput(inputFile).sorted()
        val adapters = listOf(0).plus(listFromFile).plus(listFromFile.maxOrNull()!! + 3)
        println("Adapters: $adapters")
        val validArrangements = LongArray(adapters.size) { 1 }
        for (index: Int in range(1, adapters.size)) {
            var sum = 0L
            for (srcIndex: Int in range(max(0, index-3), index)) {
                if (adapters[index] - adapters[srcIndex] <= 3) {
                    sum += validArrangements[srcIndex]
                }
                validArrangements[index] = sum
            }
        }
        val result = validArrangements[adapters.size-1]
        for (idx:Int in validArrangements.indices) {
            println("$idx = ${validArrangements[idx]}")
        }
        println("Number of valid arrangements: $result")
        return result
    }

}