package no.rokaas.adventofcode2020.day02

import java.io.File

fun main() {
    Day02().solve()
}

class Day02 {
    fun solve() {
        val input = getInput()
        println(input)
        val c1 = input.count { it.isValid1() }
        println("$c1 valid passwords, part 1")
        val c2 = input.count { it.isValid2() }
        println("$c2 valid passwords, part 2")
    }

    private fun getInput(): List<PasswordLine> {
        return File(Day02::class.java.getResource("/day02.txt").toURI()).readLines()
            .map { PasswordLine.parse(it) }
    }
}
