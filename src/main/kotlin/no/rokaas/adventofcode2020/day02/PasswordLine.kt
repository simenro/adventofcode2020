package no.rokaas.adventofcode2020.day02

import java.util.regex.Pattern

data class PasswordLine(val line: String, val min: Int, val max: Int, val letter: String, val pwd: String = "") {

    companion object {
        fun parse(line: String): PasswordLine {
            val seq = line.splitToSequence("-", " ", ": ")
            val it = seq.iterator()
            return PasswordLine(
                line,
                Integer.parseInt(it.next()),
                Integer.parseInt(it.next()),
                it.next(),
                it.next()
            )
        }
    }

    fun isValid1(): Boolean {
        return pwd.count { i -> i == letter[0] } in min..max
    }

    fun isValid2(): Boolean {
        val idx = pwd.indicesOf(letter)
        val res = idx.contains(min) xor idx.contains(max)
        if (res) {
            println("Valid: $line")
        }
        return res
    }

    // Extension method
    private fun CharSequence.indicesOf(input: String): List<Int> =
        Regex(Pattern.quote(input)) // build regex
            .findAll(this)    // get the matches
            .map { it.range.first + 1 } // get the index
            .toCollection(mutableListOf()) // collect the result as list
}
