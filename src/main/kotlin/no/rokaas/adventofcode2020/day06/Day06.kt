package no.rokaas.adventofcode2020.day06

import java.io.File

fun main() {
    Day06().solve()
}

class Day06 {
    fun solve() {
        var groups = getInput(anyone = true)
        println("Num groups: ${groups.size}")
        println("Sum 'anyone' counts: ${count(groups)}")
        groups = getInput(anyone = false)
        println("Sum 'everyone' counts: ${count(groups)}")
    }

    private fun count(groups: List<Group>): Int {
        var sumCounts = 0
        val sumby = groups.sumBy { it.getNumberOfUniqueAnswers() }
        println("umb=$sumby")
        groups.forEach {
            //println("${it.numPeople}: ${it.yesAnswers} = ${it.getNumberOfUniqueAnswers()}")
            sumCounts += it.getNumberOfUniqueAnswers()
        }
        return sumCounts
    }

    private fun getInput(anyone: Boolean): List<Group> {
        val lines = File(Day06::class.java.getResource("/day06.txt").toURI()).readLines()
        val groups = ArrayList<Group>()
        val answerLines = ArrayList<String>()
        lines.forEach { line ->
            if (line.isEmpty()) {
                groups.add(Group.parse(answerLines, anyone))
                answerLines.clear()
            } else {
                answerLines.add(line)
            }
        }
        if (answerLines.isNotEmpty()) {
            groups.add(Group.parse(answerLines, anyone))
        }
        return groups
    }

}