package no.rokaas.adventofcode2020.day06

data class Group(val lines: List<String>, val numPeople: Int, val yesAnswers: List<Char>) {

    companion object {
        fun parse(lines: List<String>, anyone: Boolean): Group {
            return if (anyone) {
                // combine (any) answers from anyone
                val combinedAnswers = lines.joinToString("").toCharArray().distinct().sorted()
                Group(lines, lines.size, combinedAnswers)
            } else {
                // combine (same) answers from everyone
                val answerCounts = HashMap<Char, Int>()
                lines.forEach { line ->
                    line.toCharArray().forEach { ch ->
                        answerCounts[ch] = answerCounts[ch]?.plus(1) ?: 1
                    }
                }
                val commonAnswers = answerCounts.filter { it.value == lines.size }.keys.toList()
                Group(lines, lines.size, commonAnswers)
            }
        }
    }

    fun getNumberOfUniqueAnswers(): Int {
        return yesAnswers.size
    }
}
